## Wallpaper

一款使用WPF制作的仿Wallpaper动态壁纸软件

## 实现原理

将视频窗体置于图标层下方,即可实现动态壁纸

参考文章: [Wallpaper的原理和C#实现](https://blog.dearxuan.com/2021/07/27/Wallpaper%E7%9A%84%E5%8E%9F%E7%90%86%E5%92%8CC-%E5%AE%9E%E7%8E%B0/)

## 截图

![图片](/Res/example.png)