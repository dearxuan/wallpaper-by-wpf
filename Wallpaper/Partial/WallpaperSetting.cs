﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Wallpaper.Partial
{
    [Serializable]
    public class WallpaperSetting
    {
        public string LastPlayedVideoPath { get; set; }

        /// <summary>
        /// 从配置文件中读取设置信息
        /// </summary>
        public static WallpaperSetting ReadFromFile(string file)
        {
            FileStream fileStream = null;
            try
            {
                fileStream = new FileStream(file, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                WallpaperSetting setting = (WallpaperSetting)binaryFormatter.Deserialize(fileStream);
                return setting;
            }
            finally
            {
                fileStream?.Dispose();
            }
        }

        /// <summary>
        /// 写入到配置文件,若不存在则会创建,若存在则会覆盖
        /// </summary>
        public void WriteToFile(string file)
        {
            FileStream fileStream = null;
            try
            {
                fileStream = new FileStream(file, FileMode.Create);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fileStream, this);
            }
            finally
            {
                fileStream?.Dispose();
            }
        }
    }
}
