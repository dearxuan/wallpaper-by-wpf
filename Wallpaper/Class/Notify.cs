﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wallpaper.Class
{
    public static class Notify
    {
        private static NotifyIcon notifyIcon = new NotifyIcon()
        {
            BalloonTipText = Value.ApplicationName,
            Icon = Resource.wallpaper,
            ContextMenuStrip = GetMenu()
        };

        /// <summary>
        /// 添加右下角角标
        /// </summary>
        public static void AddNewNotify()
        {
            notifyIcon.MouseClick += OnNotifyClick;
            notifyIcon.MouseDoubleClick += OnNotifyClick;
            notifyIcon.Visible = true;
        }

        private static void OnNotifyClick(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                notifyIcon.ContextMenuStrip.Hide();
                Static.mainWindow.Show();
            }
        }

        private static ContextMenuStrip GetMenu()
        {
            const int Item_Window = 0;
            const int Item_Play = 1;
            const int Item_Pause = 2;
            const int Item_Stop = 3;
            const int Item_Exit = 4;

            ContextMenuStrip cms = new ContextMenuStrip();

            ToolStripMenuItem item_window = new ToolStripMenuItem("主界面") { Tag = Item_Window };
            item_window.Click += OnMenuItemClick;

            ToolStripMenuItem item_play = new ToolStripMenuItem("播放") { Tag = Item_Play };
            item_play.Click += OnMenuItemClick;

            ToolStripMenuItem item_pause = new ToolStripMenuItem("暂停") { Tag = Item_Pause };
            item_pause.Click += OnMenuItemClick;

            ToolStripMenuItem item_stop = new ToolStripMenuItem("结束播放") { Tag = Item_Stop };
            item_stop.Click += OnMenuItemClick;

            ToolStripMenuItem item_exit = new ToolStripMenuItem("退出") { Tag = Item_Exit };
            item_exit.Click += OnMenuItemClick;

            cms.Items.Add(item_window);
            cms.Items.Add(item_play);
            cms.Items.Add(item_pause);
            cms.Items.Add(item_stop);
            cms.Items.Add(item_exit);

            return cms;

            void OnMenuItemClick(object sender, EventArgs e)
            {
                switch ((int)((ToolStripMenuItem)sender).Tag)
                {
                    case Item_Exit:
                        Static.mainWindow.Close();
                        Static.player?.Dispose();
                        Dispose();
                        try
                        {
                            Files.SaveSetting();
                        }
                        catch (Exception) { }
                        Application.Exit();
                        Environment.Exit(0);
                        break;
                    case Item_Play:
                        Static.player.Resume();
                        break;
                    case Item_Pause:
                        Static.player.Pause();
                        break;
                    case Item_Stop:
                        Static.player.Dispose();
                        break;
                    case Item_Window:
                        Static.mainWindow.Show();
                        break;
                }
            }
        }

        public static void Dispose()
        {
            notifyIcon.Visible = false;
            notifyIcon.Dispose();
        }
    }
}
