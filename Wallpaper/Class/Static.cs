﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Wallpaper.Partial;

namespace Wallpaper.Class
{
    public static class Static
    {
        public static Forms.MainWindow mainWindow = null;

        private static Forms.Player _player = null;

        public static Forms.Player player
        {
            get
            {
                if (_player == null)
                {
                    _player = new Forms.Player();
                }
                return _player;
            }
            set
            {
                _player = value;
            }
        }

        public static WallpaperSetting wallpaperSetting = Files.ReadWallpaperSetting();

        public static ImageSource Image_Error = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
            Resource.Image_Error.GetHbitmap(),
            IntPtr.Zero,
            System.Windows.Int32Rect.Empty,
            BitmapSizeOptions.FromEmptyOptions());
    }

    public static class Value
    {
        public const string ApplicationName = "Wallpaper动态壁纸";
    }
}
