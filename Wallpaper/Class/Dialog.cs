﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Wallpaper.Class
{
    public static class Dialog
    {
        public static void ShowDialog(string text)
        {
            MessageBox.Show(text, "Wallpaper");
        }

        public static string ShowDialog_ImportVideo()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "选择视频";
            dialog.Filter = "视频|*.mp4;*.wmv;*.avi|所有文件|*.*";
            if(dialog.ShowDialog() == DialogResult.OK)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        public static string ShowDialog_SelectCover()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "选择封面图";
            dialog.Filter = "图片|*.jpg;*.jpeg;*.png;*.bmp;|所有文件|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }

        public static void ShowDialog_Error(string s) 
        {
            MessageBox.Show(s, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowDialog_Exception(Exception e)
        {
            MessageBox.Show("发生了一个异常:\n" + e.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
