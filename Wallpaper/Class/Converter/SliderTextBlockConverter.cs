﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Wallpaper.Class.Converter
{
    [ValueConversion(typeof(double), typeof(int))]
    public class SliderTextBlockConverter : IValueConverter
    {
        /// <summary>
        /// Slider到TextBlock
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)(double)value;
        }

        /// <summary>
        /// TextBlock到Slider(不使用)
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)(int)value;
        }
    }
}
