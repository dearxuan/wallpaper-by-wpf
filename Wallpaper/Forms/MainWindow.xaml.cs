﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Wallpaper.Class;
using Wallpaper.Partial;

namespace Wallpaper.Forms
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public static ObservableCollection<ItemUnit> itemsUnitList = new ObservableCollection<ItemUnit>();

        private static ItemUnit _SelectItemUnit = null;

        public static ItemUnit SelectItemUnit
        {
            get => _SelectItemUnit;
            set
            {
                _SelectItemUnit = value;
                Static.wallpaperSetting.LastPlayedVideoPath = value?.VideoPath;
                Static.mainWindow.selectItemImage.Source = _SelectItemUnit?.image;
                Static.mainWindow.selectItemTitle.Text = _SelectItemUnit?.VideoTitle;
                if (value == null)
                {
                    Static.player.Dispose();
                }
                else
                {
                    Static.player.PlayNew(value.VideoPath);
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            Static.mainWindow = this;
            Files.Init();
            Notify.AddNewNotify();
            this.itemsControl.ItemsSource = itemsUnitList;
            
        }

        /// <summary>
        /// 窗体加载时
        /// </summary>
        private void OnLoad(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(() =>
            {
                this.itemsControl.Dispatcher.BeginInvoke(new Action(() =>
                {
                    List<ItemUnit> units = Files.GetVideoList();
                    ItemUnit lastPlay = null;
                    bool flag = true;
                    foreach (ItemUnit item in units)
                    {
                        itemsUnitList.Add(item);
                        if (flag && item.VideoPath == Static.wallpaperSetting.LastPlayedVideoPath)
                        {
                            lastPlay = item;
                            flag = false;
                        }
                    }
                    SelectItemUnit = lastPlay;
                }));
            });
            thread.Start();
        }

        /// <summary>
        /// 添加项目,可以在非UI线程调用
        /// </summary>
        public void AddIntoList(string dir)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                itemsUnitList.Add(Files.GetItemUnit(dir));
            }));
        }

        /// <summary>
        /// 项目点击事件
        /// </summary>
        private void OnItemClick(object sender, MouseButtonEventArgs e)
        {
            if(e.ChangedButton == MouseButton.Left)
            {
                ItemUnit unit = ((Grid)sender).Tag as ItemUnit;
                SelectItemUnit = unit;
            }
        }

        /// <summary>
        /// 按钮点击事件
        /// </summary>
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if(button == Button_Import)
            {
                new VideoImportForm().ShowDialog();
            }
            else if(button == Button_OpenResourceDir)
            {
                System.Diagnostics.Process.Start(Files.VideoFileDir);
            }
        }

        /// <summary>
        /// 菜单点击事件
        /// </summary>
        private void OnMenuItemClick(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            ItemUnit unit = (ItemUnit)item.Tag;
            switch (item.Name)
            {
                case "MenuItem_PlayThisVideo":
                    SelectItemUnit = (ItemUnit)item.Tag;
                    break;
                case "MenuItem_OpenInFolder":
                    System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(unit.VideoPath));
                    break;
                case "MenuItem_DeleteThisVideo":
                    if(SelectItemUnit == unit)
                    {
                        SelectItemUnit = null;
                    }
                    itemsUnitList.Remove(unit);
                    System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(unit.VideoPath), true);
                    break;
            }
        }

        private void OnImageMouseEnter(object sender, MouseEventArgs e)
        {
            Image image = (Image)sender;
            ItemUnit unit = (ItemUnit)image.Tag;
            unit.StartScaleUpAnimation(image);
        }

        private void OnImageMouseLeave(object sender, MouseEventArgs e)
        {
            Image image = (Image)sender;
            ItemUnit unit = (ItemUnit)image.Tag;
            unit.StartScaleDownAnimation(image);
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void OnSliderSpeedDragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            Player.Speed = (int)this.Slider_Speed.Value;
        }
    }
}
