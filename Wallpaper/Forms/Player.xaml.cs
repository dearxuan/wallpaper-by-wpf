﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Wallpaper.Class;

namespace Wallpaper.Forms
{
    /// <summary>
    /// Player.xaml 的交互逻辑
    /// </summary>
    public partial class Player : Window
    {
        /// <summary>
        /// 是否已置底
        /// </summary>
        private bool IsBottom = false;

        /// <summary>
        /// 正在播放的视频地址
        /// </summary>
        private string PlayingVideoPath = null;

        /// <summary>
        /// 时间线
        /// </summary>
        private MediaTimeline mTimeLine = null;

        /// <summary>
        /// 时钟
        /// </summary>
        private MediaClock mClock = null;

        private static double _speed = 1.0;

        private static double _volume = 0.5;

        /// <summary>
        /// 修改播放速度,范围为 [10,200],默认100
        /// </summary>
        public static int Speed { get
            {
                return (int)(_speed * 100);
            }
            set
            {
                if (value >= 10 && value <= 200)
                {
                    _speed = value / 100.0;
                    if(Static.player?.mClock != null)
                    {
                        Static.player.mClock.Controller.SpeedRatio = _speed;
                    }
                }
            }
        }

        /// <summary>
        /// 修改音量,范围为 [0,100],默认50
        /// </summary>
        private static int volume { get
            {
                return (int)(_volume * 100);
            }
            set
            {
                if(value >= 0 && value <= 100)
                {
                    _volume = value / 100.0;
                    Static.player.MediaPlayer.Volume = _volume;
                }
            }
        }

        /// <summary>
        /// 当前状态
        /// </summary>
        public State state = State.Hide;

        public Player()
        {
            InitializeComponent();
            this.MediaPlayer.Volume = _volume;
        }

        private void SetBottom()
        {
            if (IsBottom)
            {
                return;
            }
            this.Left = this.Top = 0;
            this.Width = SystemParameters.PrimaryScreenWidth;
            this.Height = SystemParameters.PrimaryScreenHeight;
            this.Show();
            this.state = State.None;
            this.IsBottom = FormsControl.SetBottom(this);
            if (!IsBottom)
            {
                this.Hide();
                this.state = State.Error;
                throw new Exception("创建播放层失败");
            }
            else
            {
                
            }
        }

        /// <summary>
        /// 播放新的视频
        /// </summary>
        /// <param name="path">视频路径</param>
        public bool PlayNew(string path)
        {
            if(this.PlayingVideoPath != path)
            {
                try
                {
                    SetBottom();
                    mTimeLine = new MediaTimeline(new Uri(path, UriKind.Relative));
                    mTimeLine.RepeatBehavior = RepeatBehavior.Forever;
                    mClock = mTimeLine.CreateClock();
                    mClock.Controller.SpeedRatio = _speed;
                    this.PlayingVideoPath = path;
                    this.MediaPlayer.Clock = mClock;
                    this.state = State.Playing;
                    return true;
                }
                catch (Exception)
                {
                    
                }
            }
            return false;
        }

        /// <summary>
        /// 继续播放
        /// </summary>
        public bool Resume()
        {
            if (this.state == State.Pause)
            {
                mClock.Controller.Resume();
                this.state = State.Playing;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 暂停
        /// </summary>
        public bool Pause()
        {
            if(this.state == State.Playing)
            {
                mClock.Controller.Pause();
                this.state = State.Pause;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 摧毁窗体
        /// </summary>
        /// <returns></returns>
        public void Dispose()
        {
            this.Hide();
            mClock?.Controller.Stop();
            this.MediaPlayer.Close();
            this.Close();
            Static.player = null;
            FormsControl.RefreshBackground();
        }
    }

    public enum State
    {
        /// <summary>
        /// 未展现
        /// </summary>
        Hide = 0,

        /// <summary>
        /// 未开始播放视频
        /// </summary>
        None = 1,

        /// <summary>
        /// 正在播放视频
        /// </summary>
        Playing = 2,

        /// <summary>
        /// 正在暂停
        /// </summary>
        Pause = 3,

        /// <summary>
        /// 遇到错误
        /// </summary>
        Error = 4
    }
}
