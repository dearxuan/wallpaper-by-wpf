﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Wallpaper.Class;
using Wallpaper.Partial;

namespace Wallpaper.Forms
{
    /// <summary>
    /// VideoImportForm.xaml 的交互逻辑
    /// </summary>
    public partial class VideoImportForm : Window
    {
        public VideoImportForm()
        {
            InitializeComponent();
        }

        private void OnVideoPathButtonClick(object sender, RoutedEventArgs e)
        {
            string result = Dialog.ShowDialog_ImportVideo();
            if(result != null)
            {
                this.text_VideoPath.Text = result;
                if (string.IsNullOrWhiteSpace(text_Title.Text))
                {
                    this.text_Title.Text = Path.GetFileNameWithoutExtension(result);
                }
            }
        }

        private void OnCoverPathButtonClick(object sender, RoutedEventArgs e)
        {
            string result = Dialog.ShowDialog_SelectCover();
            if (result != null)
            {
                this.text_CoverPath.Text = result;
            }
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            string path = text_VideoPath.Text;
            string cover = text_CoverPath.Text;
            string title = text_Title.Text;
            //检查视频文件是否存在
            if (!File.Exists(path))
            {
                Dialog.ShowDialog_Error("无效的视频路径");
                return;
            }
            //检查封面图文件是否存在
            if(checkBox_UseCustomCover.IsChecked == true && !File.Exists(cover))
            {
                Dialog.ShowDialog_Error("无效的封面图路径");
                return;
            }
            //检查标题
            if (string.IsNullOrWhiteSpace(title))
            {
                title = Path.GetFileNameWithoutExtension(path);
            }
            //导入视频
            if(checkBox_UseCustomCover.IsChecked != true)
            {
                cover = null;
            }
            new Thread(() =>
            {
                string dir = Files.ImportVideoFromDevice(path, title, cover);
                if (dir == null)
                {
                    Dialog.ShowDialog_Error("导入失败");
                    return;
                }
                else
                {
                    Static.mainWindow.AddIntoList(dir);
                }
            }).Start();
            this.Close();
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
